import React from 'react'
import { connect } from 'react-redux'
import keyBy from 'lodash/keyBy'
import partial from 'lodash/partial'

import Timetable from '../components/Timetable'
import { PUMPKIN_VALID_ACTIONS } from '../state'

const mapStateToProps = state => ({
	timetables: state.timetables,
})

const mapDispatchToProps = dispatch => ({
	onAction: (date, action) => {
		if (!PUMPKIN_VALID_ACTIONS [action]) throw new Error ('Invalid action '+action)
		dispatch ({ type: 'PUMPKIN_ACTION', action, date })
	},

	onTimetableMissing: date => dispatch ({ type: 'TIMETABLES_GET', date }),
})

class TimetableContainer extends React.Component {
	componentDidMount() {
		this.componentWillReceiveProps (this.props)
	}

	componentWillReceiveProps (nextProps) {
		const { date, timetables, onTimetableMissing } = nextProps

		if (date && !timetables [date]) {
			onTimetableMissing (date)
		}
	}

	render () {
		const { date, timetables, onAction } = this.props

		return !timetables [date] ? null :
			<Timetable
				timetable={ timetables [date] }
				onAction={ partial (onAction, date) }
			/>
	}
}

TimetableContainer = connect (mapStateToProps, mapDispatchToProps) (TimetableContainer)

export default TimetableContainer
