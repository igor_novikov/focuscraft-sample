import React from 'react'
import moment from 'moment'

import { translate } from '../i18n'
import { capitalizeSoft } from '../util'
import { timetableStatus, timetableTotalPumpkins, timetableTotalTime, timetableMicrosessions, pumpkinExpectedEnd } from '../state.js'
import PanelHeader from './PanelHeader'
import Microsession from './Microsession'
import Countdown from './Countdown'
import ButtonRow from './ButtonRow'

const Timetable = ({ t, timetable, onAction }) => {
	const status = timetableStatus (timetable)
	const microsessions = timetableMicrosessions (timetable)
	const totalPumpkins = timetableTotalPumpkins (timetable)
	const totalTime = timetableTotalTime (timetable)
	const lastPumpkin = timetable.pumpkins [timetable.pumpkins.length - 1] || null
	const shouldPlaySound = FocusCraft.config.userSettings [(status === 'incomplete' ? 'pumpkin' : 'shortBreak')+'TimerSound']

	let countdownProps
	if (status === 'incomplete') {
		countdownProps = {
			from: lastPumpkin.begin,
			to: pumpkinExpectedEnd (lastPumpkin),
			playSound: shouldPlaySound,
			iconColor: 'OrangeRed',
		}
	} else if (status === 'short_break' || status === 'long_break') {
		countdownProps = {
			from: lastPumpkin.end,
			duration: FocusCraft.config.userSettings [status === 'short_break' ? 'shortBreakLength' : 'longBreakLength'].asMilliseconds(),
			playSound: shouldPlaySound,
			iconColor: 'DarkGreen',
		}
	} else {
		countdownProps = {}
	}

	const neighbourDayUrl = (delta) => moment (timetable.date).add ('day', delta).format ('/YYYY-MM-DD')

	return <div className="center-outer"> <div className="center-middle"> <div className="center-inner">
		<PanelHeader
			className="landscape-only"
			header={ capitalizeSoft (moment (timetable.date).locale (t('/localeCode')).format (t('dateFormat'))) }
			prevUrl={ neighbourDayUrl (-1) }
			nextUrl={ neighbourDayUrl (+1) }
		/>

		{ totalPumpkins > 0 &&
			<p className="totals">
				<img className="icon" src="img/complete.png" alt={ t('pumpkinTotal') } aria-label={ t('pumpkinTotalAria') } />
				<span>{ totalPumpkins }</span>
				<img className="icon" src="img/time-worked.png" alt={ t('timeTotal') } aria-label={ t('timeTotalAria') } />
				<span>{ moment.utc (totalTime.asMilliseconds()).format ('H:mm') }</span>
			</p>
		}

		<div className="landscape-only">
			<Countdown {...countdownProps} />
			<ButtonRow status={ status } onButtonClick={ onAction } />
		</div>

		<table className="microsessions"><tbody>
			{ microsessions.map ((microsession, index) => <Microsession key={ index } pumpkins={ microsession } />) }
		</tbody></table>
	</div> </div> </div>
}

export default translate ({ Timetable })
