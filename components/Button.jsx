import React from 'react'
import partial from 'lodash/partial'

const Button = ({ type, text, image, href, onButtonClick }) =>
	<a
		className={ 'button btn-'+type.replace ('/', '-') }
		href={ href || 'javascript:;' }
		onClick={ partial (onButtonClick, type) }>

		{ image ?
			<img src={ image } alt={ text } aria-label={ text } />
		:
			text
		}
	</a>

export default Button
