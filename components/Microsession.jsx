import React from 'react'
import moment from 'moment'

import Pumpkin from './Pumpkin'

const Microsession = ({ pumpkins }) => {
	const begin = pumpkins [0].begin, end = pumpkins [pumpkins.length - 1].end

	return <tr>
		<td className="begin_time">{ moment (begin).format ('H:mm') }</td>
		<td className="icons">
			{ pumpkins.map (pumpkin => <Pumpkin key={ pumpkin.id } pumpkin={ pumpkin } />) }
		</td>
		<td className="end_time">{ end === null ? '--:--' : moment (end).format ('H:mm') }</td>
	</tr>
}

export default Microsession
