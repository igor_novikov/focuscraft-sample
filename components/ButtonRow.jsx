import React from 'react'

import Button from './Button'

import { translate } from '../i18n'

const ButtonRow = ({ t, status, onButtonClick }) =>
	<div className="button-row">
		{
			[{
				type:   'undo',
				text:   t('undo'),
				image:  'img/undo.png',
				status: ['short_break', 'long_break', 'closed', 'incomplete'],
			}, {
				type:   'break/short',
				text:   t('breakShort'),
				image:  null,
				status: ['long_break', 'closed'],
			}, {
				type:   'break/long',
				text:   t('breakLong'),
				image:  null,
				status: ['short_break', 'closed'],
			}, {
				type:   'begin',
				text:   t('begin'),
				image:  null,
				status: ['short_break', 'long_break', 'none', 'closed'],
			}, {
				type:   'break/none',
				text:   t('breakNone'),
				image:  'img/done.png',
				status: ['short_break', 'long_break'],
			}, {
				type:   'interrupt',
				text:   t('interrupt'),
				image:  null,
				status: ['incomplete'],
			}, {
				type:   'end',
				text:   t('end'),
				image:  null,
				status: ['incomplete'],
			}, {
				type:   'overdrive',
				text:   t('overdrive'),
				image:  null,
				status: ['incomplete'],
			}]
			.filter (button => _.indexOf (button.status, status) !== -1)
			.map (button => <Button key={ button.type } onButtonClick={ onButtonClick } {...button} />)
		}
	</div>

export default translate ({ ButtonRow })
