import React from 'react'
import moment from 'moment'
import c from 'classnames'

import { clamp } from '../util'
import { notifySound, updateIcon } from '../fx'

export default class Countdown extends React.Component {
	constructor() {
		super()
		this.state = {
			isActive: false,
			elapsed: null,
			progress: null,
			timeLeft: null,
		}
	}

	componentDidMount() {
		this.componentWillReceiveProps (this.props)
		this.setState (state => {
			state.intervalId = setInterval (this.tick.bind (this), 1000)
			return state
		})
	}

	componentWillUnmount() {
		updateIcon()
		clearInterval (this.state.intervalId)
	}

	componentWillReceiveProps (props) {
		if (props.from && (props.to || props.duration)) {
			this.setState (state => {
				Object.assign (state, {
					isActive: true,
					progress: null,
					timeLeft: null,
					duration: props.duration || moment (props.to).diff (props.from),
					from: props.from,
				})
				return this.tickState (state, props)
			})
		} else {
			updateIcon()
			this.setState (state => Object.assign (state, { isActive: false }))
		}
	}

	tick() {
		this.setState (this.tickState.bind (this), () => {
			if (this.state.isJustFinished) {
				updateIcon()
				if (this.props.playSound) notifySound.play()
				if (this.props.onDone) this.props.onDone()
			}
		})
	}

	tickState (state, props) {
		props = props || this.props

		const
			{ from, duration } = state,
			elapsed = moment().diff (from),
			progress = clamp (elapsed / duration),
			timeLeft = clamp (duration - elapsed, 0, +Infinity),
			isJustFinished = state.progress < 1 && progress === 1

		return Object.assign (state, { progress, elapsed, timeLeft, isJustFinished })
	}

	render() {
		const { isActive, progress, elapsed, timeLeft } = this.state

		if (timeLeft > 0 && this.props.iconColor) {
			updateIcon (timeLeft / 1000, this.props.iconColor)
		}

		const timerWidth = (isActive ? 100 - 100 * progress : 0)+'%'

		return <div className={ c("timer-widget", { inactive: !isActive, pending: progress < 1, complete: progress >= 1 }) }>
			<div className="timer-caption">
				<span className="remaining">
					{ formatTime (progress < 1 ? timeLeft : 0) }
				</span>

				<span className="elapsed">
					(<span>{ formatTime (clamp (elapsed, 0, +Infinity)) }</span>)
				</span>
			</div>
			<div className="timer-bar" style={{ width: timerWidth }}>
				&nbsp;
			</div>
		</div>
	}
}

const formatTime = time => moment (time).format (time >= 60 * 60 * 1000 ? 'HH:mm:ss' : 'mm:ss')
