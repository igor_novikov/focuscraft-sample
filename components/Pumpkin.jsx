import React from 'react'
import moment from 'moment'
import { pumpkinStatus } from '../state'

const Pumpkin = ({ pumpkin }) => {
	const { begin, end } = pumpkin, status = pumpkinStatus (pumpkin)

	return <span>
		<img
			src={ `img/${status}.png` }
			alt={ { complete: 'V', interrupt: 'X', incomplete: '...' } [status] }
			title={ `[${ moment (begin).format ('H:mm') } - ${ end === null ? '...' : moment (end).format ('H:mm') }]` }
		/><span className="spacing"> </span>
	</span>
}

export default Pumpkin
