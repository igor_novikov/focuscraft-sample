## [FocusCraft](http://focuscraft.org)

A complete, functional time management application based on the Pomodoro technique. It's a personal project I've implemented during the few months I took off from freelancing. I'm using it on the daily basis myself, and it really helps me every day to focus on my work. I'm still developing it, hoping to make it public soon.

Here are some of its highlights:

- Localization -- currently available in English and Russian.
- Tutorial -- When the user logs in for the first time, they are presented with a tutorial to help them learn both the Pomodoro Technique and the app's own UI.
- Undo feature -- user can easily correct their mistakes.
- Performance -- it loads fast and any action is performed almost instantly.
- Guest mode -- user doesn't have to register right away, they can create a guest account to try out the application and later convert it into a regular account.
- Fairly good cross-browser support.

Frontend is implemented with React and Redux. Async actions are handled with Redux-Saga, routing with React Router and localization with react-i18next. It is built with Webpack and Babel. Backend is implemented in PHP and MySQL with the Slim framework, and database migrations are performed by Liquibase.

I would prefer not to share the full source code, but I'm sharing a small subset of it for examination. I hope this is enough for you to get familiar with my coding style for both the stateless components managed by Redux and stateful components (`components/Countdown.js`, the countdown timer that is only displaying the time left until the end of a cycle and not interacting with the state in any meaningful way).

You can also view the application at http://focuscraft.org
